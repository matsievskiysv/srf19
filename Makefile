# Config file
MKRC ?= latexmkrc

# Source .tex file
SOURCE ?= paper

# LaTeX compiler output .pdf file
TARGET ?= $(SOURCE)

# indent options
INDENT_SETTINGS ?= indent.yaml
INDENT_FILES ?= $(wildcard *.tex)

# LaTeX version:
# -pdf		= pdflatex
# -pdfdvi	= pdflatex with dvi
# -pdfps	= pdflatex with ps
# -pdfxe	= xelatex with dvi (faster than -xelatex)
# -xelatex	= xelatex without dvi
# -pdflua	= lualatex with dvi  (faster than -lualatex)
# -lualatex	= lualatex without dvi
BACKEND ?= -pdfxe

LATEXFLAGS ?= -halt-on-error -file-line-error
LATEXMKFLAGS ?= -silent
BIBERFLAGS ?= # --fixinits
REGEXDIRS ?= . Dissertation Synopsis Presentation # distclean dirs
TIMERON ?= 1 # show CPU usage

# Makefile options
MAKEFLAGS := -s
.DEFAULT_GOAL := all
.NOTPARALLEL:

export LATEXFLAGS
export BIBERFLAGS
export REGEXDIRS
export TIMERON

##! compile all
all: paper poster

define compile
	latexmk -norc -r $(MKRC) $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) $(SOURCE)
endef

%.fmt: %.tex
	etex -ini -halt-on-error -file-line-error \
	-shell-escape -jobname=$(TARGET) \
	"&latex" mylatexformat.ltx """$^"""

##! compile paper
paper: TARGET=paper
paper: SOURCE=paper
paper:
	$(compile)

##! compile poster
poster: TARGET=poster
poster: SOURCE=poster
poster:
	$(compile)

##! indent *.tex files
indent:
	@$(foreach file, $(INDENT_FILES),\
	latexindent -l=$(INDENT_SETTINGS) -s -w $(file);)

##! preformat all
preformat: paper-preformat poster-preformat

##! preformat paper
paper-preformat: TARGET=paper
paper-preformat: paper.fmt paper

##! preformat poster
poster-preformat: TARGET=poster
poster-preformat: poster.fmt poster

# https://gist.github.com/klmr/575726c7e05d8780505a
##! this message
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^##! / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^##! //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n##! /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')

##! clean temporary files for TARGET
clean-target:
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) -c $(SOURCE)

##! full clean for TARGET
distclean-target:
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) -C $(SOURCE)

##! clean project temporary files
clean:
	"$(MAKE)" SOURCE=paper TARGET=paper clean-target

##! full clean project
distclean:
	"$(MAKE)" SOURCE=paper TARGET=paper distclean-target

.PHONY: all paper clean-target indent preformat paper-preformat \
help distclean-target clean distclean
