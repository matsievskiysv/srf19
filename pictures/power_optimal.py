#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

LINEWIDTH = 5
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

Q0 = 1.3e9  # добротность
Q0 = Q0/2  # реальная будет примерно в 2 раза ниже
Qls = [np.logspace(6,7.5),np.logspace(6.2,7.2),np.logspace(5.8,6.8),np.logspace(5.4,5.8)]  # нагруженная добротность
beta_b = 0.21  # относительная фазовая скорость
df = np.array([0,10,20,30])  # смещение частоты, вызванное силой Лоренса, давлением гелия и т.д., Гц
f0 = 325e6  # резонансная частота, МГц
Eacc = 6.4e6  # ускоряющий градиент, В/м
l = 0.188  # длина ячейки, м
# Rsh/Q0 = 302 Ом
Rsh = 302 * Q0  # шунтовое эффективное сопротивление, Ом/м
Ib = np.array([1e-6,5e-4,2e-3,1e-2])  # ток пучка, А
# betalambda = beta_b * 299792458 / f0
phase_s = np.pi/9  # синхронная фаза
Vacc_des = 6.4e6*0.188  # ускоряющее напряжение в ячейке, В

imin = {}  # положения минимумов
for n, i in enumerate(Ib):
    plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
    Ql = Qls[n]
    beta = Q0/Ql-1  # коэффициент связи
    Qex = Q0 / beta
    imin[i] = {}
    for delta in df:
        Pg = Vacc_des**2/(4*Rsh*Qex/Q0)*((1+i*Rsh*Qex*np.cos(phase_s)/Q0/Vacc_des)**2+(2*Qex*(delta)/f0)**2)
        plt.plot(Ql,Pg, linewidth=LINEWIDTH)
        imin[i][delta] = {"Pg":Pg[np.argmin(Pg)], "Ql": Ql[np.argmin(Pg)]}
        print(f"Ток {i} А, смещение частоты {delta} Гц. Минимальное значение мощности на графике {imin[i][delta]['Pg']:.2e} при внешней добротности {imin[i][delta]['Ql']:.2e}")
    plt.xscale('log')
    plt.grid()
    plt.legend([f"$\\Delta$f = {f} Гц" for f in df])
    plt.title(f"$I_b$={i} А")
    plt.xlabel("$Q_{ex}$")
    plt.ylabel("$P_g$, Вт")
    plt.tight_layout()
    plt.savefig(f"power_Ql_I{i}".replace(".","-") + ".png")
    #plt.show()


Ql = Qls[3]
beta = Q0/Ql-1  # коэффициент связи
Qex = Q0 / beta
qpos = imin[0.01][30]["Ql"]  # оптимальная связь для тока 0,01А с расстройкой 30Гц
qposi = np.where(Qex>=qpos)[0][0]
for delta in df:
    plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
    for i in Ib:
        Pg = (Vacc_des**2/(4*Rsh*Qex/Q0)*((1+i*Rsh*Qex*np.cos(phase_s)/Q0/Vacc_des)**2+(2*Qex*(delta)/f0)**2))
        plt.plot(Ql,Pg,linewidth=LINEWIDTH)
        print(f"Ток {i} А, смещение частоты {delta} Гц. Требуемая подводимая мощность {Pg[qposi]:.2e}")
    plt.axvline(qpos)
    plt.xscale('log')
    plt.grid()
    plt.title(f"$\Delta$f = {delta}")
    plt.legend([f"$I_b$={i} А" for i in Ib])
    plt.xlabel("$Q_{ex}$")
    plt.ylabel("$P_g$, Вт")
    plt.tight_layout()
    plt.savefig(f"power_Ql_f{delta}".replace(".","-") + ".png")
    #plt.show()

I = np.logspace(-8,-2)
beta_opt = Rsh * I / Vacc_des + 1
Pg = Vacc_des ** 2 * (I * Rsh / Vacc_des + 1) / Rsh
# plt.xscale('log')
plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
ax = plt.gca()
ax2 = ax.twinx()
ax.set_yscale('log')
ax.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xscale('log')
ax.plot(I, beta_opt, linewidth=LINEWIDTH)
ax2.plot(I, Pg, linewidth=LINEWIDTH)
ax.set_xlabel(r"$I_b$, А")
ax.set_ylabel(r"$\beta_{opt}$")
ax2.set_ylabel(r"$P_{opt}$, Вт")
ax.grid()
plt.tight_layout()
plt.savefig(f"power_beta".replace(".","-") + ".png")
# plt.show()
