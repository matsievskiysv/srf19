#!/bin/bash

FILE_NAME=""
FILE_EXT=""
FUZZ=5

for f in "$@"; do
    if [ ! -f "$f" ]; then
	echo File "$f" not found >&2
	continue
    fi

    FILE_NAME=${f%.*}
    FILE_EXT=${f##*.}

    mv "$f" "$FILE_NAME"_ORIGINAL."$FILE_EXT"

    convert "$FILE_NAME"_ORIGINAL."$FILE_EXT" -fuzz "$FUZZ" -transparent white "$FILE_NAME.$FILE_EXT"
done

